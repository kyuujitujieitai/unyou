# -*- coding: utf8 -*-

import sys
import tkinter as tk
import threading
import concurrent.futures
import datetime
#import drew
from apscheduler.schedulers.blocking import BlockingScheduler
scdl = BlockingScheduler()


# 運用メインの変数
unyouDnmList = [1, 2, 3, 4, 5, 6]

#運用
un1 = '[1]'
un2 = '[2]'
un3 = '[3]'
un4 = '[4]'
un5 = '[5]'
un6 = '[6]'

# 現在時刻
nowHour = datetime.datetime.now().hour
nowMin = datetime.datetime.now().minute
def nowHourDef():
    global nowHour
    nowHour = datetime.datetime.now().hour
def nowMinDef():
    global nowMin
    nowMin = datetime.datetime.now().minute

#　時刻変化処理
stateDict = {
    8:[36, 24, 12, 0, -12, -24],
    9:[24, 12, 0, -12, -24, -36],
    10:[12, 0, -12, -24, -36, 24],
    11:[0, -12, -24, -36, 24, 12],
    12:[-12, -24, -36, 24, 12, 0],
    13:[-24, -36, 24, 12, 0, -12],
    14:[-36, 24, 12, 0, -12, -24],
    15:[24, 12, 0, -12, -24, -36],
    16:[12, 0, -12, -24, -36, 24],
    17:[0, -12, -24, -36, 24, 12],
    18:[-12, -24, -36, 24, 12, 0],
    19:[-24, -36, 24, 12, 0, -12],
    20:[-36, 24, 12, 0, -12, -24],
    }

def timeState():
    global unyouDnmList
    nowHourDef()
    unyouDnmList = stateDict.get(nowHour)
    print(str(unyouDnmList) + 'timeState')

def add1min():
    global unyouDnmList
    unyouDnmList = [i+1 if i < 36 else i*(-1)+1 for i in unyouDnmList]

# 駆動用
#@scdl.scheduled_job('interval',minutes=1)
def timed_job():
    nowMinDef()
    if nowMin != 0:
        add1min()
        print(unyouDnmList)
        print(nowMin)
    else:
        timeState()
    w.coords(un1d, iti(1), umiYma(1))
    w.coords(un2d, iti(2), umiYma(2))
    w.coords(un3d, iti(3), umiYma(3))
    w.coords(un4d, iti(4), umiYma(4))
    w.coords(un5d, iti(5), umiYma(5))
    w.coords(un6d, iti(6), umiYma(6))
    w.after(60000, timed_job)

#起動時の処理とスケジューラーの起動
timeState()
for i in range(nowMin):
    add1min()
print(str(unyouDnmList) + "init")
#if __name__ == "__main__":
#scdl.start()


#描画処理
root = tk.Tk()
root.title(u"here is title")
root.geometry("800x450")
#座標群
wWidth = 800
wHeight = wWidth / 16 * 9  #450
wMid = wHeight / 2  #225
lLeng = 36 * 20  #720
lSX = (wWidth - lLeng ) / 2  #40
lSY = wMid  #225
lEX = wWidth - lSX  #760
lEY = wMid  #225

w = tk.Canvas(root, width=wWidth,  height=wHeight)
# 背景
w.create_line(lSX, lSY, lEX, lEY, fill="#111111")
def base(num):
    return int(num) * 20 + lSX
terminal = [0,36]
for i in terminal:
    w.create_rectangle(base(i)-5, wMid-4, base(i)+5, wMid+4,  fill = 'white', stipple = 'gray25')
double = [6, 12, 18, 24, 30]
for i in double:
    w.create_oval(base(i)-4, wMid-4, base(i)+4, wMid+4,  fill = 'white', stipple = 'gray25')
single = [2, 4, 9, 14, 16, 21, 27, 32, 34]
for i in single:
    w.create_line(base(i), wMid-4, base(i), wMid+4)
# 背景おまけ
w.create_oval(100, 450-200, 700, 450+200,  fill = 'lightsteelblue1', outline = 'steelblue4')
w.create_oval(280-30, 310-25, 280+30, 310+25,  fill = 'DarkOliveGreen2', outline = 'tan4')

# 列車
def iti(i):
    return abs(unyouDnmList[i-1]) * 20 + lSX
def umiYma(i):
    if unyouDnmList[i-1] < 0 or unyouDnmList[i-1] == 36:
        return wMid+10
    else:
        return wMid-10
un1d = w.create_text(iti(1), umiYma(1), text=un1)
un2d = w.create_text(iti(2), umiYma(2), text=un2)
un3d = w.create_text(iti(3), umiYma(3), text=un3)
un4d = w.create_text(iti(4), umiYma(4), text=un4)
un5d = w.create_text(iti(5), umiYma(5), text=un5)
un6d = w.create_text(iti(6), umiYma(6), text=un6)

w.place(x=0, y=0)
timed_job()
#ここまで描画処理
root.mainloop()


# 資料
ekidictA = {'01':'藤沢', '02':'石上', '03':'柳小路', '04':'鵠沼', '05':'湘南海岸公園', '06':'江ノ島', '07':'腰越', '08':'鎌倉高校前', '09':'七里ヶ浜', '10':'稲村ヶ崎', '11':'極楽寺', '12':'長谷', '13':'由比ヶ浜', '14':'和田塚', '15':'鎌倉'}

ekiDictB = {0:'藤沢', 2:'石上', 4:'柳小路', 6:'鵠沼', 9:'湘南海岸公園', 12:'江ノ島', 14:'腰越', 16:'鎌倉高校前', 18:'峰ヶ原', 21:'七里ヶ浜', 24:'稲村ヶ崎', 27:'極楽寺', 30:'長谷', 32:'由比ヶ浜', 34:'和田塚', 36:'鎌倉'}

ekiDctC = {'fu':0, 'ku':6, 'en':12, 'mi':18, 'in':24, 'ha':30, 'ka':36}

ekiDictD = {0:'藤沢', 1:'石上', 2:'柳小路', 3:'鵠沼', 4:'湘南海岸公園', 5:'江ノ島', 6:'腰越', 7:'鎌倉高校前', 8:'峰ヶ原', 9:'七里ヶ浜', 10:'稲村ヶ崎', 11:'極楽寺', 12:'長谷', 13:'由比ヶ浜', 14:'和田塚', 15:'鎌倉'}
